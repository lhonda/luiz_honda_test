#!/usr/bin/env python

import pytest
from question_a import overlap

overlap_fixture = [
    ((1, 3), (5, 8), False),
    ((0, 4), (2, 6), True),
    ((3, -1), (6, 4), False),
    ((0, 0), (6, 4), False),
]


@pytest.mark.parametrize("range_a, range_b, expected",
                         overlap_fixture)
def test_overlap(range_a, range_b, expected):
    assert overlap(range_a, range_b) == expected
