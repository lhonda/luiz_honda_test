#!/usr/bin/env python

import pytest
from question_b import check_version

versions_fixture = [
    ("1.1", "1.2", "<"),
    ("0.1", "1.1", "<"),
    ("2.0", "1.9", ">"),
    ("2.0", "2.0", "="),
    ("2.19", "2.2", ">"),
]


@pytest.mark.parametrize("version_a, version_b, expected",
                         versions_fixture)
def test_check_versions(version_a, version_b, expected):
    assert check_version(version_a, version_b) == expected
