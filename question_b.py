#!/usr/bin/env python

"""
Question B
The goal of this question is to write a software library that accepts 2 version string as input
and returns whether one is greater than, equal, or less than the other.
As an example: “1.2” is greater than “1.1”. Please provide all test cases you could think of.

"""


def _compare_ints(a, b):
    result = '='
    if a > b:
        result = '>'
    elif a < b:
        result = '<'
    return result


def check_version(version_a, version_b):
    int_a, fraction_a = map(int, version_a.split('.'))
    int_b, fraction_b = map(int, version_b.split('.'))
    result = _compare_ints(int_a, int_b)

    if result == '=':
        fraction_result = _compare_ints(fraction_a, fraction_b)
        result = fraction_result
    return result
