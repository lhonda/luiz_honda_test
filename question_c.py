#!/usr/bin/env python

"""

"""
from urllib.parse import quote

import requests
from bs4 import BeautifulSoup


def request_get():
    return requests.get


class MissingDependencyError(Exception):
    pass


def _get_deps():
    return {
        'headers': {'user-agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0'},
        'html_parser': BeautifulSoup,
        'request_get': request_get,
    }


def get_results(keyword='keystone - Circular reference found role inference', limit=5, deps={}):
    for k in deps.keys():
        if not deps[k]:
            raise MissingDependencyError()

    if ' ' in keyword:
        keyword = quote(keyword)

    url = f'https://google.com/search?q={keyword}'
    response = deps['request_get'](url, headers=deps['headers'])
    soup = deps['html_parser'](response.content, 'html5lib')

    divs = soup.find_all('div', {"class": "rc"})

    results = []
    import ipdb;ipdb.set_trace()
    for index, item in enumerate(divs):
        if index == limit:
            break
        result = {
            'title': item.find('h3').text,
            'link': item.find('cite').text,
            'comment': item.find('span', {"class": "st"}).text,
        }
        results.append(result)

    return {'data': results}


if __name__ == '__main__':
    deps = _get_deps()
    results = get_results(deps=deps)
    from pprint import pprint

    pprint(results)
