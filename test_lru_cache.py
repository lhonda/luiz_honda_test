#!/usr/bin/env python

"""
Thus, your challenge is to write a new Geo Distributed LRU (Least
Recently Used) cache with time expiration. This library will be used extensively by many of our
services so it needs to meet the following criteria:
 
    1 - Simplicity. Integration needs to be dead simple.
    2 - Resilient to network failures or crashes.
    3 - Near real time replication of data across Geolocation. Writes need to be in real time.
    4 - Data consistency across regions
    5 - Locality of reference, data should almost always be available from the closest region
    6 - Flexible Schema
    7 - Cache can expire 
As a hint, we are not looking for quantity, but rather quality, maintainability, scalability,
testability and a code that you can be proud of. 
When submitting your code add the necessary documentation to explain your overall design
and missing functionalities.  Do it to the best of your knowledge.

Draft:

1. Simplicity:

My first idea is to provide an API interface with get, set functions; 
get method decorated by functools.lru_cache;


2. Create (or use some discovery server, like Consul) in several aws regions. Discovery server must have:
   *) register/unregister node 
   *) health check
   *) list nodes

Create a lambda function to retrieve the cached data and persist on DynamoDB (every delta t); 
on cache service reload, retrieve the saved cache from DynamoDB; 

3 and 4. Cache service retrieves a list of nodes from discovery server and writes into the other nodes.

5. Provided by aws LB
6. use dict to save data
7. provide TTL to set function

Missing functionalities:
upgrade to websocket, smaller payload
"""



