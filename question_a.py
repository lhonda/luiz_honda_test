#!/usr/bin/env python

"""
Question A
Your goal for this question is to write a program that accepts two lines (x1,x2) and (x3,x4)
on the x-axis and returns whether they overlap.

As an example, (1,5) and (2,6) overlaps but not (1,5) and (6,8).
"""


def _convert_tuple_to_set(t):
    s = sorted(t)
    return set(range(s[0], s[1] + 1))


def overlap(range_a, range_b):
    set_a = _convert_tuple_to_set(range_a)
    set_b = _convert_tuple_to_set(range_b)
    result = len(set_a.intersection(set_b))
    return result != 0
